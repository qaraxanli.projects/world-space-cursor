# World Space Cursor 
A must-have Unity plugin for pointer-driven world space UI. Especially necessary for XR content.

![Demo Gif](https://i.imgur.com/1c3bCfY.gif "Demo Gif")

Although the regular system cursor works with Unity's world space canvases, it's drawn in screen space. Aside from this being bad UX, this approach doesn't work at all for XR.
<br>
With this plugin, your system cursor is completely replaced with a "World Space Cursor". That is, the cursor is located in the same 3D space as your canvas. It has the same orientation and scale as your canvas, so it always looks great no matter how your canvas is positioned.
<br>
- Works with every Unity UI element based on Selectable: buttons, text fields, sliders, toggles, and scrollbars. Dropdowns can work but require customization, see Disclaimers below.<br>
- Will work with any custom Selectable you create, too!<br>
- Works with the standard Unity Input system, which means that gamepads, keyboards, and mouse controls work side-by-side<br>
- Comes with 3 simple cursor images, and a .psd template to easily create your own custom cursors<br>

### Installation
Just download or clone the repository and place the World Space Cursor folder into your project's Assets folder.

### How to Use World Space Cursor 
There are 2 ways to use the World Space Cursor in your project.
<br><br>**1) Create a new canvas and UI using the pre-configured WSC Canvas**
<br><br>This is the simplest option. All you have to do is click the Create button (or right click in the hierarchy tab) and choose UI->WSC - Canvas to add the canvas object to your scene (you can also drag the WSC Canvas prefab from the "World Space Cursor/Resources" root folder into your scene instead if you wish). Then you can set up your UI just as you normally would in Unity. After doing this, just check out the customization options below and you'll be ready to go!
<br><br>**2) Integrate the World Space Cursor into your existing world space canvas UI**
<br><br>First, click the Create button in the hierarchy tab (or right click) and choose UI->WSC  - World Space Cursor. Make the World Space Cursor object you just added a child of your canvas object, and remember to make it the last object in the hierarchy underneath your canvas so that it renders on top of everything else. Next, click the Create button in the hierarchy tab (or right click) to add the UI->WSC EventSystem object into your scene as well (it shouldn't matter where it's located in the hierarchy).
<br><br>Note: both the World Space Cursor and the WSC EventSystem prefabs are located in the "World Space Cursor/Resources" folder, along with the WSC Canvas prefab. 

### Customization Options 
The "World Cursor" script on the World Cursor object (child of the WSC Canvas if using option 1 above) has four options for customizing your World Space Cursor.
<br>**Cursor Xaxis:** The name of the Unity Input axis that controls the relative left and right movement of the cursor. By default this is set to "Mouse X", which means World Space Cursor will move left or right when the mouse moves left or right.
<br>**Cursor Yaxis:** The name of the Unity Input axis that controls the relative up and down movement of the cursor. By default this is set to "Mouse Y", which means World Space Cursor will move up or down when the mouse moves up or down.
<br>**Select Button:** The button name (from the [Unity Input options](https://docs.unity3d.com/Manual/ConventionalGameInput.html)) for selecting UI elements with the cursor. By default it's "Fire1", which is bound to the left mouse button for most setups. 
<br>**Sensitivity Factor:** This is multiplied into the calculation for how fast the cursor should move around. It defaults to 5, and higher = faster.
### Disclaimers
**Colliders**<br>
World Space Cursor works, in part, by setting up 2D box colliders on all the selectable objects in the scene and testing for collision with the cursor object. It does its best to determine the bounds of each selectable UI element and shape the colliders accordingly, but it may not work perfectly if your UI has elements with strange shapes. If there are any issues with the collision detection for your UI, just attach a 2D collider to the objects in your UI and customize the size yourself. Any selectable object without a 2D collider will be set-up automatically at runtime, but those with one already will be skipped.
<br><br>**Dropdowns**
<br>Unity's default dropdown implementation uses a secondary canvas, which isn't compatible with World Space Cursor. However, you can create your own dropdown objects using other standard Unity UI elements like buttons and images, and that will work fine with World Space Cursor.
### Contribution
This project was initially released on the Unity Asset Store in 2015. It was maintained for quite some time but has never been my primary focus. Therefore, I'm releasing it as open-source in case others find it useful.
If you use this in your project, please let me know and consider crediting me! I'd love to see what you're doing with it. Merge requests are welcome!